import java.math.BigDecimal;
import java.util.*;

public class CalculatorApp {

    public static void main(String[] args) throws Exception {

        String strVal = "11.0 - ( ( 15 / 5 ) * 2 )";
        System.out.println("count : " + strVal);
        System.out.println("result: " + calculate(strVal));
    }

    public static double calculate(String sum) {
        // System.out.println(sum);
        String[] item = sum.split(" ");
        Stack<Integer> bracketIdx = new Stack<>();
        List<String> itemArrList = new ArrayList<>();

        for (int i = 0; i < item.length; i++) {
            itemArrList.add(item[i]);
            if ("(".equals(item[i])) {
                bracketIdx.push(i);
            } else if (")".equals(item[i])) {
                int openIdx = bracketIdx.pop();
                List<String> strVal = itemArrList.subList(openIdx, itemArrList.size());
               // System.out.println("val: " + strVal);
                double count = getCalculationOutput(strVal);
                strVal.clear();
                itemArrList.add(String.valueOf(count));
            }
            //System.out.println("calc status: " + itemArrList.toString());
        }
       // System.out.println("final calc: " + itemArrList.toString());
        double result = getCalculationOutput(itemArrList);
        //System.out.println("final result: " + result);
        return result;
    }


    public static double getCalculationOutput(final List<String> xp) {
        List<String> eq = new ArrayList<>(xp);
        //System.out.println("Str list: " + xp);
        //System.out.println("last char: " + expr.get(eq.size() - 1));

        if (xp.get(0).equals("(") && xp.get(eq.size() - 1).equals(")")) {
            eq = new ArrayList<>(xp.subList(1, xp.size() - 1));
        }

        Queue<String> addSub = new LinkedList<>();
        BigDecimal left = null;
        String operatorStr = null;
        for (int i = 0; i < eq.size(); i++) {
            try {
                BigDecimal num = new BigDecimal(eq.get(i));
                //System.out.println("num left: " + num);
                if (left == null) {
                    left = num;
                } else {
                    if (operatorStr.equals("*")) {
                        left = left.multiply(num);
                    } else if (operatorStr.equals("/")) {
                        left = left.divide(num);
                    } else if (operatorStr.equals("+") || operatorStr.equals("-")) {
                        addSub.add(left.toPlainString());
                        addSub.add(operatorStr);
                        left = num;
                    }
                }
            } catch (NumberFormatException e) {
                operatorStr = eq.get(i);
            }
        }

        addSub.add(left.toPlainString());
        BigDecimal result = BigDecimal.ZERO;
        operatorStr = "+";
        while (!addSub.isEmpty()) {
            String item = addSub.remove();
            try {
                BigDecimal right = new BigDecimal(item);
                if ("+".equals(operatorStr)) {
                    result = result.add(right);
                } else if ("-".equals(operatorStr)) {
                    result = result.subtract(right);
                }
            } catch (NumberFormatException e) {
                operatorStr = item;
            }
        }

        // System.out.println("result = " + result);
        return result.doubleValue();
    }
}
